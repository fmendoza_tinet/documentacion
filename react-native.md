# Organización de un proyecto en React Native

## 1. Estructura por defecto

+ **android/** -- Directorio donde reside el codigo nativo de Android. Acá se encuentran archivos _.gradle_, _.java_ y _.xml_. Este directorio se puede utilizar directo desde el _"Android Studio"_. No debiera ser necesario efectuar cambios manuales en este directorio.

+ **ios/** -- Directorio donde reside el codigo nativo de iOS. Acá se encuentra el proyecto xcode, con archivos _.plist_, _.h_, _.m_, etc. Si quieres abrir este proyecto con xcode, se debe usar el archivo _ios/<NOMBRE_PROYECTO>.xcodeproj_. No debiera ser necesario efectuar cambios manuales en este directorio.

+ **index.ios.js** -- Es el punto de entrada de la aplicacion react native. Es donde se registra la aplicacion (vía _AppRegistry_).

+ **index.android.js** -- Lo mismo que _index.ios.js_ solo que para Android.

+ **Todo lo demás** -- Practicamente no es necesario modificar los dem´ñas archivos -- En su mayoría son archivos de configuración para _React Native_ y el resto son los archivos estandar de node (_node_modules_ y _package.json_).

## 2. El directorio app/

Después de revisar la estructura base, procedemos a revisar con más detalle cada uno de los directorios importantes. El punto de partida de la aplicación, es el directorio _app/_. Para tener una idea de como debiera ser tenemos el siguiente ejemplo:

``` bash
─ app
  ├── components
  ├── config
  ├── lib
  ├── screens
  └── index.js
```

El directorio _app/_ está al mismo nivel que los directorios _ios/_ y _android/_. Dentro del directorio _app/_ es donde se encuentra toda la lógica de la aplicación. Esta dividido en una serie de directorios para organizar y localizar mas fácilmente el código. A continuación veamos cada uno de esos directrios internos.

+ ### __app/components__

  Este directorio esta enfocado en la _Máxima reutilización de código_ y _Minimizar estados de componentes_. En este directorio se ubican los componentes funcionales que se utilizarán en la aplicación. Seguido, un ejemplo de como debiera verse un directorio _components/_ en una aplicación.

  ``` bash
  └─── Avatar
  │    ├─── Avatar.js
  │    ├─── index.js
  │    └─── styles.js
  ├─── GenericTextinput
  │    ├─── GenericTextinput.js
  │    ├─── InputWrapper.js
  │    ├─── index.js
  │    └─── styles.js
  └─── Loading
       ├─── Loading.js
       ├─── index.js
       └─── styles.js
  ```

  El archivo _index.js_ esta encargado de definir que se exporta en el directorio. _Avatar.js_ es el componente en sí (Componente funcional). Y por ultimo _styles.js_ se encarga de definir los estilos del componente.

  El proposito de mantener los estilos fuera del archivo de componente, es simplemente el poder maximizar reutilización, como en _GenericTextInput_.

+ ### __app/config/__

  Este directorio se encarga de mantener la _configuración fuera del código_. La idea bajo esto, es mantener las configuraciones en un lugar común para poder accederlos facilmente y poder modificar una vez. Seguido, un ejemplo de como debiera verse este directorio.

  ``` bash
  ─ config
    ├─── routes.js
    ├─── settings.js
    └─── styles.js
  ```

  Acá se puede ver un archivo _routes.js_ que nos entrega un punto unico para la navegacion de la aplicacion, el archivo _settings.js_ contiene informacion como la URL del servidor y _styles.js_ que es un archvo que contiene los estilos mas globales de la aplicación.

  Esto nos genera un punto unico para manejar la configuracion de la aplicacion.

+ ### __app/lib/__

  Este direcrotio refuerza la reutilizacion de codigo y multi plataforma. En este directorio se deben incluir funciones genericas. Se debe tener especial atencion en investigar antes si existe una libreria npm antes de crear algo en este directorio.

+ ### __app/screens/__

  En este directorio se encuentran todas las vistas de la aplicación, Una aplicación esta compuesta por _pantallas_ (o si prefieres _rutas/pantallas_). Por cada _pantalla/ruta/pantalla_ que existe en el archivo _routes.js_ debiera existir un archivo en el directorio _screens_.

  Estas _pantallas_ son escencialmente "pequeños" contenedores que se encargan de manejar los datos obtenidos, interacciones, etc. La *UI* esta delegada a varios componentes de presentación que existen en el directorio _components_. Esto significa, que muy rara vez se debe tener configuración de estilos en este directorio. Los estilos deben manejarse en el componente.

  Seguido, un ejemplo del diretorio _screens_

  ``` bash
  ─ screens
    ├─── Details.js
    ├─── Home.js
    ├─── Profile.js
    └─── SignIn.js
  ```

+ ### __app/index.js__

  Finalmente el unico archivo que existe en el directorio _app_ y que se utuliza como punto de partida de la aplicacion multi plataforma. Esto nos permite centralizar los archivos _index.ios.js_ e _index.android.js_ para desplegar lo mismo en ambas plataformas.

## 3. Extender el directorio app/

  La descripción anterior define lo básico para un proyecto en _React Native_. Si se desea crear una aplicación mas _grande_, se debe pensar en usar _**Redux**_.

  En ese caso se debe extender el directorio _app_ con los siguientes directorios extra.

+ _app/actions/_ -- Se deben dejar las acciones _redux_ aquí.

+ _app/reducers/_ -- Los reducers de Redux deben ir aquí.

+ _app/config/store.js_-- Almacenar el store _Redux_ aquí.
