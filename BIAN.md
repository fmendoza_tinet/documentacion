# BIAN (_Banking Industry Architecture Network_)

## Resúmen ejecutivo

BIAN es una asociación sin fines de lucro que establece y promueve un _framework arquitectonico_ para establecer interoperabilidad en la banca.

Se encarga de definir un _*framework semántico*_ para identificar y definir servicios IT en la industria bancaria. Esta alineado con SOA y se ido adaptando incluso con implementaciones con microservicios.

Define las entidades y sus relaciones. Es básicamente el analisis que debiera hacerse con DDD, pero aplicado de forma general a cualquier sistema bancario.

Cuenta con diagramas que definen los flujos, pasos e interacciones que se deben aplicar.

Esta pensado para implementar con micro servicios (tienen diagramas que indican como debe utilizarse). Incluso definen las estrategias de migración que pueden usarse.

![Estrategias de migración](images/estrategias.png)

![Integraciones mediante microservicios](images/microservicios.png)

Abarca desde servicios, negocio y operaciones. Es bien completo y varios bancos lo están usando como base para su negocio.

![BIAN](images/bian.png)

![BIAN](images/bian2.png)

Personalmente pienso que podríamos usarlo, pero hay HARTO por estudiar. En este momento van en la versión 6 del estándar (Disponible para todos) y se puede participar de sus reuniones, pero hay que pagar.. y no es barato. El participar en las reuniones es para poder opinar en el estándar y estar al día con lo ultimo antes de que salga publicado.

        How much is the BIAN annual membership fee?

        Banks (except Federal Banks & Central Banks) and other financial institutions that are not Software or Technology Service Providers pay an annual membership fee in an amount of 20.000,00 EURO.
        Federal Banks & Central Banks pay an annual membership fee in an amount of 10.000,00 EURO.
        Software or Technology Service Providers with > 249 employees pay an annual membership fee in an amount of 30.000,00 EURO.
        Software or Technology Service Providers with < 249 employees pay an annual membership fee in an amount of 10.000,00 EURO.
        Software or Technology Service Providers with <50 employees pay an annual membership fee in an amount of 5.000,00 EURO.
        Depending on the admission of membership, BIAN offers a pro-rata calculation.
